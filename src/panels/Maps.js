import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Search from '@vkontakte/vkui/dist/components/Search/Search';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import HorizontalScroll from '@vkontakte/vkui/dist/components/HorizontalScroll/HorizontalScroll';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import FixedLayout from '@vkontakte/vkui/dist/components/FixedLayout/FixedLayout';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';

import { YMaps, Map, Placemark, GeolocationControl, ZoomControl, Clusterer } from "react-yandex-maps";

import './main.css';
import cluster from './cluster.png';

const osName = platform();

const coordinates = [{
	geometry: [57.684758, 39.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8}}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [57.784758, 39.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8}}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [57.884758, 38.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8}}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [59.984758, 40.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8 }}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [59.974758, 40.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8 }}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [59.964758, 40.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8 }}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [59.954758, 40.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8 }}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}, {
	geometry: [59.954758, 42.738521],
	options: {
		iconColor: '#ffffff'
	},
	properties: {
		iconContent: '<Avatar size={64} style={{ marginBottom: 8 }}>😷</Avatar>',
		iconCaption: 'Карантин'
	}
}];

const Maps = ({ id, go }) => (
	<Panel id={id}>
		<PanelHeader>
			Карта настроения
		</PanelHeader>
		<Div>
			<YMaps>
				<Map style={{position: 'fixed', width: '100%', height: '100%'}}
					defaultState={{
                        center: [55.751574, 37.573856],
                        zoom: 5,
                    }}
				>
					<Clusterer
						options={{
                            preset: 'islands#invertedVioletClusterIcons',
                            groupByCoordinates: false,
                            clusterIcons: [
                                {
                                    href: cluster,
                                    size: [100, 100],
                                    offset: [-50, -50]
                                }]
                        }}
					>
                        {coordinates.map((value, index) => (
							<Placemark key={index} {...value} onClick={go} data-to='resultsnippet'/>
                        ))}
					</Clusterer>
				</Map>
			</YMaps>
			<div className={"bottom_block"}>
			<Search/>
				<Group style={{ paddingBottom: 8 }}>
					<HorizontalScroll>
						<FixedLayout vertical="bottom" filled={true}>
							<Search/>
							<Div>
								<div style={{ display: 'flex' }}>
									<div style={{paddingLeft: 4, paddingRight: 16, textAlign: 'center' }}>
										<Avatar size={64} style={{ marginBottom: 8}}>😃</Avatar>
										Весело
									</div>
									<div style={{paddingRight: 16, textAlign: 'center' }}>
										<Avatar size={64} style={{ marginBottom: 8 }}>😌</Avatar>
										Скучно
									</div>
									<div style={{paddingRight: 16, textAlign: 'center'  }}>
										<Avatar size={64} style={{ marginBottom: 8 }}>😜</Avatar>
										Игриво
									</div>
								</div>
							</Div>
						</FixedLayout>
					</HorizontalScroll>
				</Group>
			</div>
		</Div>
	</Panel>
);

Maps.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default Maps;
