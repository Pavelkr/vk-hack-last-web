import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS } from '@vkontakte/vkui';
import View from '@vkontakte/vkui/dist/components/View/View';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import InfoRow from '@vkontakte/vkui/dist/components/InfoRow/InfoRow';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Progress from '@vkontakte/vkui/dist/components/Progress/Progress';
import Card from '@vkontakte/vkui/dist/components/Card/Card';
import FormLayout from '@vkontakte/vkui/dist/components/FormLayout/FormLayout';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import './Snippet.css';

const osName = platform();


const ResultSnippet = ({ id, go }) => (
	<Panel id={id}>
		<View activePanel="new-user">
			<Panel id="new-user">
				<PanelHeader
					left={<PanelHeaderButton onClick={go} data-to="maps">
                        {osName === IOS ? <Icon28ChevronBack/> : <Icon24Back/>}
					</PanelHeaderButton>}
				>
					Запись
				</PanelHeader>
				<FormLayout>
					<Div>
						<Card size="l">
							<img src="https://nw.com.ua/wp-content/uploads/2020/08/8-2.jpg" alt="Qurantine"/>
							<Div>
								<div class="fee_info">
									<span class="fee_sub_header">Кабинет министров усиливает контроль за соблюдением карантина. В частности, правительство вводит ежедневную работу государственной комиссии ТЭБ и ЧС из-за ухудшения эпидситуации, сообщает правительственный портал.
									</span>
								</div>
							</Div>
						</Card>
					</Div>
				</FormLayout>
			</Panel>
		</View>
	</Panel>
);

ResultSnippet.propTypes = {
	id: PropTypes.string.isRequired,
	go: PropTypes.func.isRequired,
};

export default ResultSnippet;

