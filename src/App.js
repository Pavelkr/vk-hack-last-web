import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import '@vkontakte/vkui/dist/vkui.css';

import Maps from './panels/Maps';
import ResultSnippet from './panels/ResultSnippet';

const App = () => {
	const [activePanel, setActivePanel] = useState('maps');

	useEffect(() => {
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = data.scheme ? data.scheme : 'client_light';
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
	}, []);

	const go = e => {
        setActivePanel(e.currentTarget.dataset.to);
    };

	const go2 = e => {
        setActivePanel('resultsnippet');
    };

	return (
		<View activePanel={activePanel}>
			<ResultSnippet id='resultsnippet' go={go} />
			<Maps id='maps' go={go2} />
		</View>
	);
}

export default App;

